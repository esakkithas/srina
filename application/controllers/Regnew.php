<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Regnew extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()  {
		parent:: __construct();
		
		$this->load->model('reg_new_model');
	}
	public function index()
	{
		redirect(site_url('/Accounts/login'));
		if(isset($_POST) && !empty($_POST)){
			// echo "babu"; print_r($_POST); exit;
			$fname = $this->input->post('fname');
			$pass = $this->input->post('pass');

			if($_FILES['image']['size']!=0){
					$filename2 = "TB-".date('dmyhis').".".pathinfo($_FILES['image']['name'],PATHINFO_EXTENSION);

					if(isset($filename2)){
						$this->load->library('upload', array(
							'upload_path'=>'uploads/',
							'file_ext_tolower'=>TRUE,
							'allowed_types'=>'jpg|jpeg|png|doc|pdf|docx|xls|xlsx',
							'file_name'=>$filename2,
						));
					}
				
					if ($this->upload->do_upload('image')){
						$upload_data = $this->upload->data();
					}
			}else{
				if(@$updateimage!=""){
					$filename2 =  $updateimage;
				}else{					
					$filename2 = 'no-img.jpg';					
				}
			}



			$data = array(
				'fname' => $fname,
				'pass' => $pass,
				'image' => $filename2,
			);


			$insert_id = $this->reg_new_model->insert('reg_form',$data);
			if(isset($insert_id) && $insert_id != ""){ ?>
				<script type="text/javascript">alert("Regards saved successfully");</script>
			<?php }
		}

		$data['reg_data'] = $this->reg_new_model->select('reg_form');
		
 
		$this->load->view('reg_form',$data);
	}

	public function edit($id){
		$data['stu_data'] = $this->reg_new_model->selectrow($id);
		$this->load->view('edit_form',$data);
	}

	public function update(){
		// echo"testbabau"; print_r($_POST); exit;
		$updateid = @$this->input->post('updateid');
		$updateimage = @$this->input->post('updateimage');
		$fname = @$this->input->post('fname');
		$pass = @$this->input->post('pass');

		if($_FILES['image']['size']!=0){

					$filename2 = "TB-".date('dmyhis').".".pathinfo($_FILES['image']['name'],PATHINFO_EXTENSION);

					if(isset($filename2)){
						$this->load->library('upload', array(
							'upload_path'=>'uploads/',
							'file_ext_tolower'=>TRUE,
							'allowed_types'=>'jpg|jpeg|png|doc|pdf|docx|xls|xlsx',
							'file_name'=>$filename2,
						));
					}
				
					if ($this->upload->do_upload('image')){
						$upload_data = $this->upload->data();
					}
				}else{
				if($updateimage!=""){
					$filename2 =  $updateimage;
				}else{					
					$filename2 = 'no-img.jpg';					
				}
				}		

		$data = array(
			'fname' => $fname,
			'pass' => $pass,
			'image' => $filename2
		);

		$this->reg_new_model->update($updateid,$data); 
		redirect(base_url());
	}

	public function delete($id){
		$this->reg_new_model->delete($id);
		redirect(base_url());
	}


}