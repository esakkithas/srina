<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Income extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()  {
		parent:: __construct();
		
		$this->load->model('reg_new_model');
		$isUserLoggedIn = $this->session->userdata('isUserLoggedIn'); 
		if(!$isUserLoggedIn){
			redirect('Accounts/login'); 
		}
		$is_admin = $this->session->userdata('is_admin'); 
		if($is_admin != 1){
			echo "You do not have access to view this page."; die;
		}
	}
	public function index()
	{
		$data = array();
		$data['customers'] = $this->db->where_in('status', [1,0])->get('s_customers')->result_array();
		$data['services'] = $this->db->where_in('status', [1,0])->get('s_services')->result_array();
		$data['accountsList'] = $this->db->get('s_accounts_list')->result_array();
		$this->load->view('income',$data);
	}
	public function list1()
	{
		$data = array();
		// $data['data'] = $this->db->where_in('status', [1,0])->get('s_income')->result_array();
		$sql = 'SELECT *, s_customers.name as customer_name,s_income.id as income_id  FROM s_income
				INNER JOIN s_customers ON s_customers.id=s_income.customer_id
				WHERE s_income.status in (1,0) 
				';
		$query = $this->db->query($sql);
		$data['data']=  $query->result_array();
		return print_r(json_encode($data));
	}
	
	public function delete()
	{
		$input = $this->input->post();
		$data = array();
		$this->db->where('id',$input['id']);
		$this->db->update('s_income', array('status'=>-1, 'modified_by'=>$this->session->userdata('userid')));
		
		$this->db->where('income_id',$input['id']);
		$this->db->update('s_transactions', array('status'=>-1, 'modified_by'=>$this->session->userdata('userid')));
		$data['status'] = true;
		return print_r(json_encode($data));
	}
	public function view()
	{
		$input = $this->input->post();
		$data = array();

		$sql = 'SELECT *, s_customers.name as customer_name, s_income.id as income_id   FROM s_income
		INNER JOIN s_customers ON s_customers.id=s_income.customer_id
		WHERE s_income.id = "'.$input['id'].'" 
		';
		$query = $this->db->query($sql);
		$data['data']=  $query->row_array();

		$sql1 = 'SELECT *, s_services.name as service_name FROM s_services_in_income
		INNER JOIN s_income ON s_services_in_income.income_id=s_income.id
		INNER JOIN s_services ON s_services_in_income.service_id=s_services.id
		WHERE s_income.id ="'.$input['id'].'" ;
		';
		// echo $sql1;
		$query1 = $this->db->query($sql1);
		$data['serviceslist']=  $query1->result_array();

		$sql3 = 'SELECT *  FROM s_transactions
		WHERE income_id = "'.$input['id'].' and amount > 0 and account_id=7" 
		';
		$query3 = $this->db->query($sql3);
		$data['pending']=  $query3->row_array();

		$sql4 = 'SELECT *  FROM s_transactions
		WHERE income_id = "'.$input['id'].' and amount > 0 and account_id!=7" 
		';
		$query4 = $this->db->query($sql4);
		$data['paid']=  $query4->row_array();

		return print_r(json_encode($data));
	}
	public function add()
	{
		$input = $this->input->post();

		$insert_data = array();
		$insert_data['datetime'] = $input['datetime'];
		$insert_data['customer_id'] = $input['customer_id'];
		$insert_data['bill_amount'] = $input['bill_amount'];
		$insert_data['payment_status'] = $input['payment_status'];
		$insert_data['payment_status'] = $input['payment_status'];
		$insert_data['payment_responsible_by'] = $input['payment_responsible_by'];
		$insert_data['created_by'] = $this->session->userdata('isUserLoggedIn'); 
		$this->db->insert('s_income',$insert_data);
		$income_id = $this->db->insert_id();

		foreach($input['service_name'] as $key=>$value){
			$services_in_income = array();
			$services_in_income['income_id'] = $income_id;
			$services_in_income['service_id'] = $input['service_name'][$key];
			$services_in_income['ref_no'] = $input['ref_no'][$key];
			$services_in_income['work_status'] = $input['status'][$key];
			// $services_in_income['amount'] = $input['amount'][$key];
			$services_in_income['total_amount'] = $input['total_amount'][$key];
			$services_in_income['quantity'] = $input['quantity'][$key];
			$services_in_income['created_by'] = $this->session->userdata('isUserLoggedIn'); 
			$this->db->insert('s_services_in_income',$services_in_income);
		}

		$transactions =array();
		$transactions['created_by'] = $this->session->userdata('userid'); 
		$transactions['income_id'] = $income_id;
		$transactions['datetime'] = $input['datetime'];
		$transactions['account_id'] =$input['account_id'];
		$transactions['amount'] =$input['bill_amount'];
		if($input['pending_amount'] > 0){
			$transactions['amount'] =$input['bill_amount'] - $input['pending_amount'];
		}
		if($transactions['amount'] > 0){
			$this->db->insert('s_transactions',$transactions);
		}
		if($input['pending_amount'] > 0){
			$transactions =array();
			$transactions['created_by'] = $this->session->userdata('userid'); 
			$transactions['income_id'] = $income_id;
			$transactions['datetime'] = $input['datetime'];
			$transactions['account_id'] =7;
			$transactions['amount'] =$input['pending_amount'];
			$this->db->insert('s_transactions',$transactions);
		}
		foreach($input['charges'] as $key=>$value){
			if($input['charges'][$key] > 0){
				$transactions =array();
				$transactions['created_by'] = $this->session->userdata('userid'); 
				$transactions['income_id'] = $income_id;
				$transactions['datetime'] = $input['datetime'];
				$transactions['account_id'] =$input['charges_debit_account'][$key];
				$transactions['amount'] = $input['charges'][$key] * -1;
				$this->db->insert('s_transactions',$transactions);
			}
		}

		return print_r(json_encode($input));
	}
	public function edit()
	{	
		$input = $this->input->post();

		$this->db->where('income_id',$input['income_id']);
		$this->db->where('account_id',7);
		$this->db->update('s_transactions', array('account_id'=>$input['account_id']));

		$this->db->where('id',$input['income_id']);
		$this->db->update('s_income', array('payment_status'=>1));

		return print_r(json_encode($input));
	}

}