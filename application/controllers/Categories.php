<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()  {
		parent:: __construct();
		
		$this->load->model('reg_new_model');
		$isUserLoggedIn = $this->session->userdata('isUserLoggedIn'); 
		if(!$isUserLoggedIn){
			redirect('Accounts/login'); 
		}
	}
	public function index()
	{
		$data = array();
		$this->load->view('categories',$data);
	}
	public function list2()
	{
		$data = array();
		$data['data'] = $this->db->where_in('status', [1,0]);
		$parent = null;
		if(isset($_GET['p']) && $_GET['p']>0){
			$parent = ($_GET['p']);
		}
		$data['data'] = $data['data']->where('parent_id',$parent);
		$data['data'] = $data['data']->get('s_categories')->result_array();
		return print_r(json_encode($data));
	}
	public function delete()
	{
		
		$is_admin = $this->session->userdata('is_admin'); 
		if($is_admin != 1){
			echo "You do not have access to view this page."; die;
		}
		$input = $this->input->post();
		$data = array();
		$this->db->where('id',$input['id']);
		$this->db->update('s_categories', array('status'=>-1, 'modified_by'=>$this->session->userdata('userid')));
		$data['status'] = true;
		return print_r(json_encode($data));
	}
	public function view()
	{
		$input = $this->input->post();
		$data = array();
		$this->db->where('id',$input['id']);
		$data['data'] =	$this->db->get('s_categories')->row_array();
		return print_r(json_encode($data));
	}
	
	public function add()
	{
		
		$is_admin = $this->session->userdata('is_admin'); 
		if($is_admin != 1){
			echo "You do not have access to view this page."; die;
		}
		$input = $this->input->post();
		$parent = null;
		if(isset($_GET['p']) && $_GET['p']>0){
			$parent = ($_GET['p']);
		}
		$input['parent_id'] = $parent;
		$input['created_by'] = $this->session->userdata('userid'); 
		$this->db->insert('s_categories',$input);
		return print_r(json_encode($input));
	}
	public function edit()
	{	
		
		$is_admin = $this->session->userdata('is_admin'); 
		if($is_admin != 1){
			echo "You do not have access to view this page."; die;
		}
		$input = $this->input->post();
		$input['modified_by'] = $this->session->userdata('userid'); 
		$this->db->where('id',$input['id']);
		$this->db->update('s_categories',$input);
		return print_r(json_encode($input));
	}
	
}