<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()  {
		parent:: __construct();
		
		$this->load->model('reg_new_model');
	}

	public function login()
	{
		$data = array();
 
		$this->load->view('login',$data);
	}
	public function auth()
	{
		$data = array();
		$email=$this->input->post('email');
		$password=$this->input->post('password');
		$que=$this->db->query("select * from s_users where email='$email' and password='$password'");
		$row = $que->num_rows();
		if( $row == 1)
		{
			$this->session->set_userdata('isUserLoggedIn', TRUE); 
			$this->session->set_userdata('username', $que->row()->name); 
			$this->session->set_userdata('userid', $que->row()->id); 
			$this->session->set_userdata('is_admin', $que->row()->is_admin); 
			
			$login_history = array();
			$login_history['user_id'] = $this->session->userdata('userid'); 
			$this->db->insert('s_login_history',$login_history);
			$insert_id = $this->db->insert_id();
			$this->session->set_userdata('login_history_id', $insert_id); 
			$data['status'] = "success";
		}
		else
		{
			$data['status'] = "failure";
		}
		return print_r(json_encode($data));
	}
	public function logout(){ 
		$login_history = array();
		$login_history['logout_at'] = date("Y-m-d H:i:s");
		$this->db->where('id',$this->session->userdata('login_history_id'));
		$this->db->update('s_login_history',$login_history);

        $this->session->unset_userdata('isUserLoggedIn'); 
        $this->session->sess_destroy(); 
        redirect('Accounts/login'); 
    } 

}