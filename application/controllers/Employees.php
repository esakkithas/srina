<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employees extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()  {
		parent:: __construct();
		
		$this->load->model('reg_new_model');
		$isUserLoggedIn = $this->session->userdata('isUserLoggedIn'); 
		if(!$isUserLoggedIn){
			redirect('Accounts/login'); 
		}
		$is_admin = $this->session->userdata('is_admin'); 
		if($is_admin != 1){
			echo "You do not have access to view this page."; die;
		}
	}
	public function index()
	{
		$data = array();
 
		$this->load->view('employees',$data);
	}
	public function list1()
	{
		$data = array();
		$data['data'] = $this->db->where_in('status', [1,0])->get('s_employees')->result_array();
		return print_r(json_encode($data));
	}
	public function delete()
	{
		$input = $this->input->post();
		$data = array();
		$this->db->where('id',$input['id']);
		$this->db->update('s_employees', array('status'=>-1, 'modified_by'=>$this->session->userdata('userid')));
		$data['status'] = true;
		return print_r(json_encode($data));
	}
	public function view()
	{
		$input = $this->input->post();
		$data = array();
		$this->db->where('id',$input['id']);
		$data['data'] =	$this->db->get('s_employees')->row_array();
		return print_r(json_encode($data));
	}
	public function add()
	{
		$input = $this->input->post();
		$input['created_by'] = $this->session->userdata('userid'); 
		$this->db->insert('s_employees',$input);
		return print_r(json_encode($input));
	}
	public function edit()
	{	
		$input = $this->input->post();
		$input['modified_by'] = $this->session->userdata('userid'); 
		$this->db->where('id',$input['id']);
		$this->db->update('s_employees',$input);
		return print_r(json_encode($input));
	}

}