<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expense extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()  {
		parent:: __construct();
		
		$this->load->model('reg_new_model');
		$isUserLoggedIn = $this->session->userdata('isUserLoggedIn'); 
		if(!$isUserLoggedIn){
			redirect('Accounts/login'); 
		}
		$is_admin = $this->session->userdata('is_admin'); 
		if($is_admin != 1){
			echo "You do not have access to view this page."; die;
		}
	}
	public function index()
	{
		$data = array();
		$data['assetsList'] = $this->db->where_in('status', [1,0])->get('s_assets')->result_array();
		$data['accountsList'] = $this->db->get('s_accounts_list')->result_array();
		$this->load->view('expense',$data);
	}
	public function list1()
	{
		$data = array();
		$sql = 'SELECT  *, s_assets.name as asset_name, s_expenses.id as expense_id from s_expenses
				INNER JOIN s_assets ON s_expenses.asset_id=s_assets.id 
				WHERE s_expenses.status in (1,0)
				';
		// echo $sql;die;
		$query = $this->db->query($sql);
		$data['data']=  $query->result_array();
		return print_r(json_encode($data));
	}
	public function delete()
	{
		$input = $this->input->post();
		$data = array();
		$this->db->where('id',$input['id']);
		$this->db->update('s_expenses', array('status'=>-1, 'modified_by'=>$this->session->userdata('userid')));
		$this->db->where('expense_id',$input['id']);
		$this->db->update('s_transactions', array('status'=>-1, 'modified_by'=>$this->session->userdata('userid')));
		$data['status'] = true;
		return print_r(json_encode($data));
	}
	public function view()
	{
		$input = $this->input->post();
		$data = array();
		// $this->db->where('id',$input['id']);
		// $data['data'] =	$this->db->get('s_expenses')->row_array();
		$sql = 'SELECT  *, s_assets.name as asset_name, s_expenses.id as expense_id from s_expenses
		INNER JOIN s_assets ON s_expenses.asset_id=s_assets.id 
		WHERE s_expenses.id = "'.$input['id'].'"
		';
// echo $sql;die;
$query = $this->db->query($sql);
$data['data']=  $query->row();
		return print_r(json_encode($data));
	}
	public function add()
	{
		$input = $this->input->post();
		$input['created_by'] = $this->session->userdata('userid'); 
		$account_id = $input['account_id'];
		unset($input['account_id']);
		$this->db->insert('s_expenses',$input);
		$insert_id = $this->db->insert_id();

		$transactions =array();
		$transactions['created_by'] = $this->session->userdata('userid'); 
		$transactions['expense_id'] = $insert_id;
		$transactions['datetime'] = $input['datetime'];
		$transactions['account_id'] = $account_id;
		$transactions['amount'] = -1 * abs($input['amount']);
		$this->db->insert('s_transactions',$transactions);


		return print_r(json_encode($input));
	}
	public function edit()
	{	
		$input = $this->input->post();
		$input['modified_by'] = $this->session->userdata('userid'); 
		$this->db->where('id',$input['id']);
		$this->db->update('s_expenses',$input);
		return print_r(json_encode($input));
	}

}