<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()  {
		parent:: __construct();
		
		$this->load->model('reg_new_model');
		$isUserLoggedIn = $this->session->userdata('isUserLoggedIn'); 
		if(!$isUserLoggedIn){
			redirect('Accounts/login'); 
		}
		$is_admin = $this->session->userdata('is_admin'); 
		if($is_admin != 1){
			echo "You do not have access to view this page."; die;
		}
	}
	public function index()
	{
		$data = array();
		$sql="SELECT DATE_FORMAT(datetime,'%Y-%m-%e') as transaction_date FROM s_expenses where status=1 UNION SELECT DATE_FORMAT(datetime,'%Y-%m-%e') as transaction_date FROM s_income where status=1 order by transaction_date desc";    
    	$query = $this->db->query($sql);
		$data['list'] =  $query->result_array();
		$data['t'] =  $this;
		$data['accounts'] = $this->getAccountsList();
		// $data['account_balance_details'] = array();
		// foreach($data['accounts'] as $account){
		// 	$data['account_balance_details'][$account['id']] = 0;
		// }
		$this->load->view('dashboard',$data);
	}
	public function getAccountBalance($accounts, $date){
		$account_balance_details = array();
		$account_balance_details["total"] = 0;
		foreach($accounts as $account){
			$sql="SELECT sum(amount) as amount FROM s_transactions WHERE DATE_FORMAT(datetime,'%Y-%m-%e') <= '".$date."' and account_id='".$account['id']."' and status=1";    
			$query = $this->db->query($sql);
			$data =  $query->row();
			$account_balance_details[$account['id']] = (isset($data->amount) ? $data->amount : 0);
			$account_balance_details["total"] += $account_balance_details[$account['id']];
		}
		return $account_balance_details;
	}
	public function getIncomeTotal($date){
		#$sql = "SELECT sum(bill_amount) as total,  DATE_FORMAT(datetime,'%Y-%m-%e') as d FROM s_income where  DATE_FORMAT(datetime,'%Y-%m-%e')='".$date."'  and status=1 group by d";
		$sql = "SELECT sum(amount) as total,  DATE_FORMAT(datetime,'%Y-%m-%e') as d FROM s_transactions where  DATE_FORMAT(datetime,'%Y-%m-%e')='".$date."' and income_id is not null  and status=1 group by d";
		$query = $this->db->query($sql);
		$data=  $query->row();
		
		return (isset($data) ? $data->total : "0");
	}
	public function getIncomeDetails(){
		$input = $this->input->post();
		$data = array();
		$sql = 'SELECT *, s_services.name as service_name, s_customers.name as customer_name  FROM s_services_in_income
				INNER JOIN s_income ON s_services_in_income.income_id=s_income.id
				INNER JOIN s_services ON s_services_in_income.service_id=s_services.id
				INNER JOIN s_customers ON s_income.customer_id=s_customers.id 
				WHERE DATE_FORMAT(s_income.datetime,"%Y-%m-%e")="'.$input['transactionDate'].'" and s_income.status=1 
				';
		// echo $sql;die;
		$query = $this->db->query($sql);
		$data['list']=  $query->result_array();
		$this->load->view('dashboard_view_income',$data);
	}
	public function getExpenseDetails(){
		$input = $this->input->post();
		$data = array();
		$sql = 'SELECT  *, s_assets.name as asset_name from s_expenses
				INNER JOIN s_assets ON s_expenses.asset_id=s_assets.id
				WHERE DATE_FORMAT(datetime,"%Y-%m-%e")="'.$input['transactionDate'].'" and s_expenses.status=1
				';
		// echo $sql;die;
		$query = $this->db->query($sql);
		$data['list']=  $query->result_array();
		$this->load->view('dashboard_view_expense',$data);
	}
	public function getExpenseTotal($date){
		$sql = "SELECT sum(amount) as total,  DATE_FORMAT(datetime,'%Y-%m-%e') as d FROM s_expenses where DATE_FORMAT(datetime,'%Y-%m-%e')='".$date."' and status=1 group by d";
		$query = $this->db->query($sql);
		$data=  $query->row();
		
		return (isset($data) ? $data->total : "0");
	}
	public function getAccountsList(){
		$sql = "SELECT * FROM s_accounts_list";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

}