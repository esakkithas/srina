<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Assests</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Assets</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <h3 class="card-title"></h3>
              </div>
            <div class="card">
                <h3></h3>
              <div class="container">
                <!-- Button to Open the Modal -->
           <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add">
               Add+
             </button>   
               </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="assets-tbl" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Id</th>
                    <th>name</th>
                    <th>stock</th>
                   <th>action</th> 
                  </tr>
                  </thead>
                  <tbody>
                
                  </tbody>
                  <tfoot>
                  </tfoot>
                </table>
              </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>

      <div class="modal" id="add">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Form</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <h2></h2>
<form onsubmit="addassets(); return false" method="post">
  <div class="form-group">
    <label for="uname">Name*:</label>
    <input type="" class="form-control" id="uname"  name="name" required>
    <div class="valid-feedback">Valid.</div>
  </div>
  <div class="form-group">
    <label for="pwd">Stock*:</label>
    <input type="" class="form-control" id="pwd" placeholder="" name="stock" required>
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
  </div>
  <div class="ab"> <p class="msg" style="color:green;float:left"></p>
  <input type="submit"value="submit" class="btn btn-primary"> </div>
</form>
      </div>
              </div>
              </div>
              </div>


              
      <div class="modal" id="edit">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Form</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <h2></h2>
<form onsubmit="confirmEditAssets(); return false" method="post">
  <input type="hidden" name="id">
  <div class="form-group">
    <label for="uname">Name*:</label>
    <input type="" class="form-control" id="uname"  name="name" required>
    <div class="valid-feedback">Valid.</div>
  </div>
  <div class="form-group">
    <label for="pwd">Stock*:</label>
    <input type="" class="form-control" id="pwd" placeholder="" name="stock" required>
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
  </div>
  <div class="ab"> <p class="msg" style="color:green;float:left"></p>
  <input type="submit"value="submit" class="btn btn-primary"> </div>
</form>
      </div>
              </div>
              </div>
              </div>

              <div class="modal" id="view1">
            <div class="modal-dialog">
              <div class="modal-content">
                 <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
          <td class="id"></td>
                  
                </tr>
                <tr>
                <th>Name</th>
          <td class="name"></td>
                
              </tr>
              <tr>
                <th>Stock</th>
          <td class="stock"></td>
                
              </tr>
                </thead>
                <tbody>
                  </tbody>
                  </table>
                  </div>
                  </div>
                  </div>
                  </div>


<?php $this->load->view('footer'); ?>