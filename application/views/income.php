<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>
<style>
  
#serviceslisttbl input {
  width:150px;
}
  </style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Income</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Income</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
            <h3></h3>
              <div class="rs">
                <!-- Button to Open the Modal -->
           <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add1">
               Add+  </button>
               </div>
               <!-- /.card-header -->
              <div class="card-body">
                <table id="income-tbl" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Id</th>
                    <th>datetime</th>
                    <th>Customer</th>
                    <th>Bill Amount</th>
                    <th>Payment Status</th>
                   <th>action</th> 
                  </tr>
                  </thead>
                  <tbody>
  
                  </tbody>
                  <tfoot>
                  </tfoot>
                </table>
              </div>
              <!-- The Modal -->
          
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<!-- ./wrapper -->
<div class="modal" id="add1">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add Income</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
            <!-- Modal body -->
      <div class="modal-body">
        <h2></h2>
<form onsubmit="addincome(); return false" method="post">
  <div class="form-group">
    <label for="uname">Date (mm/dd/yyyy hh:mm AM/PM)*:</label>
    <input type="datetime-local" class="form-control"  id="addincome-datetime" name="datetime" required>
    <div class="valid-feedback">Valid.</div>
  </div>
  <div class="form-group">
    <label for="pwd">Customer*:</label>
    <input list="Customerlist" id="customer-id" name="customer_id"class="custom-select mb-2" required>
  <datalist id="Customerlist">
  <?php foreach($customers as $customer): ?>
  <option  data-value="<?php echo $customer['id']; ?>" value="<?php echo $customer['name']; ?>"><?php echo $customer['name']; ?></option>
  <?php endforeach; ?>
    </datalist>
    <input type="hidden" name="customer_id" id="customer-id-hidden">
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
  </div>
  <div class="robin">
    <table id="serviceslisttbl"  class="">
      <thead>
      <tr>
        <th>Serives Name*</th>
        <th>Quantity*</th>
        <th>Charges*</th>
        <th>Debit account</th>
     
        <th>Commission*</th>
        <th>status*</th>
        <th>ref_no</th>
      </tr>
      </thead>
      <tbody>
      <tr>
        <td><input type="text" list="servicesList" class="service-name" id="service-name" required>
        <input type="hidden"  name="service_name[]" class="service-name-hidden" id="service-name-hidden">
      </td>
    
        <td><input type="number" onchange="calculation()" class="quantity"  name="quantity[]" required></td>
        <td><input type="number" onchange="calculation()" name="charges[]" class="charges"  required></td>
        <td>  <select class="form-control fees_account_id" name="charges_debit_account" >
    <option value="">Select</option>
  <?php foreach($accountsList as $acc): ?>
  <option value="<?php echo $acc['id']; ?>" <?php // echo ($acc['id']==1?'selected':''); ?> ><?php echo $acc['account_name']; ?> </option>
  <?php endforeach; ?>
  </select>
</td>
        <td><input type="number" id="pwd" class="total-amount"  onchange="calculation()" name="total_amount" required></td>
        <td>
        <select name="status[]" class="custom-select1 " required>
    <option value="">Select</option>
    <option value="Pending">Pending</option>
    <option value="Inprogress">Inprogress</option>
    <option value="Done">Done</option>
  </select>
  </td>
        <td><input type="text" class="" id="pwd" placeholder="" name="ref_no[]" ></td>
        <td> &nbsp; <a href="#" onclick="addRow(this); return false"><i class="fas fa-plus"></i></a> &nbsp;
        <a href="#" onclick="deleteRow(this); return false"><i class="fas fa-trash"></i></a> 
          </td>
         </tr>
        
        </tbody>
        </table>
        <datalist id="servicesList">
  <?php foreach($services as $service): ?>
  <option data-value="<?php echo $service['id']; ?>" data-fees_account_id="<?php echo $service['fees_account_id']; ?>" data-charges="<?php echo $service['fees']; ?>" data-comm="<?php echo $service['commission']; ?>" value="<?php echo $service['name']; ?>"><?php echo $service['name']; ?> </option>
  <?php endforeach; ?>
    </datalist>
    
</div>
      <div class="form-group">
  <label for="pwd">Total Amount*:</label>
  <input type="number" class="form-control bill-amount" id="pwd" placeholder="" name="bill_amount" required>
  <div class="valid-feedback">Valid.</div>
  <div class="invalid-feedback">Please fill out this field.</div>
</div>

<div class="form-group">
  <label for="pwd">Payment Status*:</label>
  <select name="payment_status"   class="payment_status custom-select" onchange="paymentStatusChanges()" required> 
    <option value="">Select</option>
    <option value="0">Pending</option>
    <option value="1" selected>Paid</option>
  </select>
  <div class="valid-feedback">Valid.</div>
  <div class="invalid-feedback">Please fill out this field.</div>    
</div>
<div class="form-group pending-amt">
  <label for="pwd">Pending Amount*:</label>
  <input type="number" class="form-control pending-amount" name="pending_amount" value="0" required>
  <div class="valid-feedback">Valid.</div>
  <div class="invalid-feedback">Please fill out this field.</div>
</div>
<div class="form-group payment-responsible-by">
    <label for="pwd">Payment Responsible By:</label>
    <input list="Customerlist1" class="custom-select mb-2" id="payment_responsible_by">
  <datalist id="Customerlist1">
  <?php foreach($customers as $customer): ?>
  <option  data-value="<?php echo $customer['id']; ?>" value="<?php echo $customer['name']; ?>"><?php echo $customer['name']; ?></option>
  <?php endforeach; ?>
    </datalist>
    <input type="hidden" name="payment_responsible_by" id="payment_responsible_by-hidden">
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
  </div>
<div class="form-group">
    <label for="pswd">Account*:</label>

  <select id="assetsList" class="form-control" name="account_id" requird>
    <option value="">Select</option>
  <?php foreach($accountsList as $acc): ?>
  <option value="<?php echo $acc['id']; ?>" <?php echo ($acc['account_name'] == 'Shop' ? 'selected': ''); ?>><?php echo $acc['account_name']; ?> </option>
  <?php endforeach; ?>
  </select>

  </div>
  <div class="form-group">
    <label for="uname">Paper Count(A4)*</label>
    <input type="number" class="form-control"   name="paper_count" value="1" required>
    <div class="valid-feedback">Valid.</div>
  </div>
  
    <div class="ad"><p class="msg" style="color:green;float:left"></p>
    <input type="submit"value="submit" class="btn btn-primary">
    </div>             
        </div>
        </div> 
      </form>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</div>



<!-- ./wrapper -->
<div class="modal" id="editForm">
  
<div class="modal-dialog modal-xl">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Income</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
            <!-- Modal body -->
      <div class="modal-body">
        <h2></h2>
<form onsubmit="confirmEditIncome(); return false" method="post">
<input type="hidden" name="id">
  <div class="form-group">
    <label for="uname">Date (mm/dd/yyyy hh:mm AM/PM)*:</label>
    <input type="datetime-local" class="form-control"  id="addincome-datetime" name="datetime" required disabled>
    <div class="valid-feedback">Valid.</div>
  </div>
  <div class="form-group">
    <label for="pwd">Customer*:</label>
    <input list="Customerlist" id="customer-id-edit" name="customer_id"class="custom-select mb-2" required disabled>
  <datalist id="Customerlist">
  <?php foreach($customers as $customer): ?>
  <option  data-value="<?php echo $customer['id']; ?>" value="<?php echo $customer['name']; ?>"><?php echo $customer['name']; ?></option>
  <?php endforeach; ?>
    </datalist>
    <input type="hidden" name="customer_id" id="customer-id-edit-hidden">
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
  </div>
  <div class="robin">
    <table id="serviceslisttbl"  class="">
      <thead>
      <tr>
        <th>Serives Name*</th>
        <th>Quantity*</th>
        <th>Charges*</th>
        <th>Debit account</th>
     
        <th>Commission*</th>
        <th>status*</th>
        <th>ref_no</th>
      </tr>
      </thead>
      <tbody>
      <tr>
        <td><input type="text" list="servicesList" class="service-name" id="service-name" required disabled>
        <input type="hidden"  name="service_name[]" class="service-name-hidden" id="service-name-hidden">
      </td>
    
        <td><input type="number" onchange="calculation()" class="quantity"  name="quantity[]" required disabled></td>
        <td><input type="number" onchange="calculation()" name="charges[]" class="charges"  required disabled></td>
        <td>  <select style="display:none" class="form-control fees_account_id" name="charges_debit_account" >
    <option value="">Select</option>
  <?php foreach($accountsList as $acc): ?>
  <option value="<?php echo $acc['id']; ?>" <?php // echo ($acc['id']==1?'selected':''); ?> ><?php echo $acc['account_name']; ?> </option>
  <?php endforeach; ?>
  </select>
</td>
        <td><input   type="number" id="pwd" class="total-amount"  onchange="calculation()" name="total_amount" required></td>
        <td>
        <select style="display:none"  name="status[]" class="custom-select1 " >
    <option value="">Select</option>
    <option value="Pending">Pending</option>
    <option value="Inprogress">Inprogress</option>
    <option value="Done">Done</option>
  </select>
  </td>
        <td><input type="text" class="" id="pwd" placeholder="" name="ref_no[]" disabled ></td>
        <td> &nbsp; <a href="#" style="display:none" onclick="addRow(this); return false"><i class="fas fa-plus"></i></a> &nbsp;
        <a href="#" style="display:none" onclick="deleteRow(this); return false"><i class="fas fa-trash"></i></a> 
          </td>
         </tr>
        
        </tbody>
        </table>
        <datalist id="servicesList">
  <?php foreach($services as $service): ?>
  <option data-value="<?php echo $service['id']; ?>" data-fees_account_id="<?php echo $service['fees_account_id']; ?>" data-charges="<?php echo $service['fees']; ?>" data-comm="<?php echo $service['commission']; ?>" value="<?php echo $service['name']; ?>"><?php echo $service['name']; ?> </option>
  <?php endforeach; ?>
    </datalist>
    
</div>
      <div class="form-group">
  <label for="pwd">Total Amount*:</label>
  <input type="number" class="form-control bill-amount" id="pwd" placeholder="" name="bill_amount" required disabled>
  <div class="valid-feedback">Valid.</div>
  <div class="invalid-feedback">Please fill out this field.</div>
</div>

<div class="form-group">
  <label for="pwd">Payment Status*:</label>
  <select name="payment_status"   class="payment_status-edit custom-select" onchange="paymentStatusChangesEdit()" required> 
    <option value="">Select</option>
    <option value="0">Pending</option>
    <option value="1" selected>Paid</option>
  </select>
  <div class="valid-feedback">Valid.</div>
  <div class="invalid-feedback">Please fill out this field.</div>    
</div>
<div class="form-group pending-amt-edit">
  <label for="pwd">Pending Amount*:</label>
  <input type="number" class="form-control pending-amount" name="pending_amount" value="0" required disabled>
  <div class="valid-feedback">Valid.</div>
  <div class="invalid-feedback">Please fill out this field.</div>
</div>
<div class="form-group payment-responsible-by-edit">
    <label for="pwd">Payment Responsible By:</label>
    <input list="Customerlist1" class="custom-select mb-2" id="payment_responsible_by-edit" disabled>
  <datalist id="Customerlist1">
  <?php foreach($customers as $customer): ?>
  <option  data-value="<?php echo $customer['id']; ?>" value="<?php echo $customer['name']; ?>"><?php echo $customer['name']; ?></option>
  <?php endforeach; ?>
    </datalist>
    <input type="hidden" name="payment_responsible_by" id="payment_responsible_by-edit-hidden">
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
  </div>
<div class="form-group">
    <label for="pswd">Account*:</label>

  <select id="assetsList" class="form-control" name="account_id" requird>
    <option value="">Select</option>
  <?php foreach($accountsList as $acc): ?>
  <option value="<?php echo $acc['id']; ?>"><?php echo $acc['account_name']; ?> </option>
  <?php endforeach; ?>
  </select>

  </div>
  <div class="form-group">
    <label for="uname">Paper Count(A4)*</label>
    <input type="number" class="form-control"   name="paper_count"  required disabled>
    <div class="valid-feedback">Valid.</div>
  </div>
  
    <div class="ad"><p class="msg" style="color:green;float:left"></p>
    <input type="submit"value="submit" class="btn btn-primary">
    </div>             
        </div>
        </div> 
      </form>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.col -->
</div>
</div>



<div class="modal" id="view1">
            <div class="modal-dialog">
              <div class="modal-content">
           <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <td class="id"></td>  
                </tr>
                <tr>
                  <th>Date</th>
                  <td class="datetime"></td>
                </tr>
                <tr>
                  <th>Customer</th>
                  <td class="customer"></td>
                </tr>
                <tr>
                  <th>Bill Amount</tH>
                  <td class="bill_amount"></td>
                </tr>
                <tr style="display:none">
                  <th>Pending Amount</tH>
                  <td class="pending_amount"></td>
                </tr>
                <tr>
                  <th>Payment Status</tH>
                  <td class="payment_status"></td>
                </tr>
                <tr>
                  <th>Account</tH>
                  <td class="account"></td>
                </tr>
                <tr>
                  <th>Payment Responsible By</tH>
                  <td class="payment_responsible_by"></td>
                </tr>
                </thead>
            
                  </table>
<br>
                  <table id="serviceslist" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Service Name</th>
                  <th>Amount</th>
                  <th>Status</th>
                  <th>Ref No</th>
                </tr>
               
                </thead>
                <tbody>
</tbody>
                  </table>
                
                  </div>
                  </div>
                  </div>
                  
                  </div>
                  

        <div class="modal" id="add">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Income</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
            <!-- Modal body -->
      <div class="modal-body">
        <h2></h2>
<form onsubmit="addincome(); return false" method="post">
  <div class="form-group">
    <label for="uname">Date :</label>
    <input type="date" class="form-control" id="uname" placeholder="" name="uname" required>
    <div class="valid-feedback">Valid.</div>
  </div>
  <div class="form-group">
    <label for="pwd">Customer:</label>
    <input list="browsers" name="browser"class="custom-select mb-2">
  <datalist id="browsers">
    <option value="Robin">
    <option value="vbin">
    <option value="prince">

    <input type="name" class="form-control" id="pwd" placeholder="" name="pswd" required>
    </datalist>
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
  </div>
  <div class="robin">
    <table id=""  class="">
      <thead>
      <tr>
        <th>Serives_id</th>
        <th>Amount</th>
        <th>staus</th>
        <th>ref_no</th>
      </tr>
      </thead>
      <tbody>
      <tr>
        <td><input type="name" class="" id="pwd" placeholder="" name="" required></td>
        <td><input type="name" class="" id="pwd" placeholder="" name="" required></td>
        <td>
        <select name="cars" class="custom-select mb-3">
    <option selected></option>
    <option value="pending">pending</option>
    <option value="paid">paid</option>
  </select>
  </td>
        <td><input type="name" class="" id="pwd" placeholder="" name="" required></td>
        <td><i class="fas fa-trash"></i>
          </td>
         </tr>
        <tr>
          <td><input type="name" class="" id="pwd" placeholder="" name="" required></td>
          <td><input type="name" class="" id="pwd" placeholder="" name="" required></td>
          <td> 
    <select name="cars" class="custom-select mb-3">
    <option selected></option>
    <option value="pending">pending</option>
    <option value="paid">paid</option>
  </select>
  </td>
          <td><input type="name" class="" id="pwd" placeholder="" name="" required></td>
        <td><i class="fas fa-trash"></i>

          <td></td>
        </tr>
        </tbody>
        </table>
</div>
      <div class="form-group">
  <label for="pwd">bill_Amount:</label>
  <input type="number" class="form-control" id="pwd" placeholder="" name="pswd" required>
  <div class="valid-feedback">Valid.</div>
  <div class="invalid-feedback">Please fill out this field.</div>
</div>
<div class="form-group">
  <label for="pwd">pending_Amount:</label>
  <input type="number" class="form-control" id="pwd" placeholder="" name="pswd" required>
  <div class="valid-feedback">Valid.</div>
  <div class="invalid-feedback">Please fill out this field.</div>
</div>
<div class="form-group">
  <label for="pwd">payment_status:</label>
  <select name="cars"   class="custom-select mb-3"> 
    <option selected></option>
    <option value="pending">pending</option>
    <option value="paid">paid</option>
  </select>
  <div class="valid-feedback">Valid.</div>
  <div class="invalid-feedback">Please fill out this field.</div>    
</div>
    <div class="ad"><p class="msg" style="color:green;float:left"></p>
    <input type="submit"value="submit" class="btn btn-primary">
    </div>             
        </div>
        </div> 
</form>
        </div>
<?php $this->load->view('footer'); ?>