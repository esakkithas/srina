<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Categories</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Categories</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <h3 class="card-title"></h3>
              </div>
            <div class="card">
                <h3></h3>
              <div class="container">
                <!-- Button to Open the Modal -->
           <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add" <?php echo ( $this->session->userdata('is_admin')!=1?'disabled':''); ?>>
               Add+
             </button>   
               </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="categories-tbl" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Id</th>
                    <th>Name</th>
                   <th>action</th> 
                  </tr>
                  </thead>
                  <tbody>
                 
                  </tbody>
                  <tfoot>
                  </tfoot>
                </table>
              </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <!-- The Modal -->
<div class="modal" id="add">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Form</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <h2></h2>
<form  onsubmit="addCategory(); return false" method="post">
  <div class="form-group">
    <label for="uname">name*:</label>
    <input type="uname" class="form-control" id="uname"  name="name" required>
    <div class="valid-feedback">Valid.</div>
  </div>
  <div class="ab"><p class="msg" style="color:green;float:left"></p>
  <input type="submit"value="submit" class="btn btn-primary"> </div>
</form>
      </div>
              </div>
              </div>
              </div>

              
<div class="modal" id="edit">
<input type="hidden" name="id">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Form</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <h2></h2>
<form  onsubmit="confirmEditService(); return false" method="post">
<input type="hidden" name="id">
  <div class="form-group">
    <label for="uname">name:</label>
    <input type="uname" class="form-control" id="uname"  name="name" required>
    <div class="valid-feedback">Valid.</div>
  </div>
  <div class="form-group">
    <label for="pwd">govt-fees:</label>
    <input type="number" class="form-control" id="pwd"  name="fees" required>
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
  </div>
  <div class="form-group">
    <label for="pwd">commission:</label>
    <input type="number" class="form-control" id="pwd"  name="commission" required>
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
  </div> 
  <div class="ab"><p class="msg" style="color:green;float:left"></p>
  <input type="submit"value="submit" class="btn btn-primary"> </div>
</form>
      </div>
              </div>
              </div>
              </div>
            
            <!-- The Modal -->
           <div class="modal" id="view1">
            <div class="modal-dialog">
              <div class="modal-content">
                 <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <td class="id"></td>
                </tr>
                <tr>
                <th>Name</th>
                <td class="name"></td>
              </tr>
              <tr>
                <th>Govt. Fees</th>
                <td class="fees"></td>
              </tr>
              <tr>
                <th>Commission</th>
                <td class="commission"></td>
              </tr> <tr>
                <th>Total Price</th>
                <td class="total"></td>
              </tr>
                </thead>
                <tbody>
                  </tbody>
                  </table>
                  </div>
                  </div>
                  </div>
                  </div>


<?php $this->load->view('footer'); ?>