<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <h3 class="card-title"></h3>
              </div>
              <div class="card">
                <h2></h2>
                <div class="container">
                  <!-- Button to Open the Modal -->               
              </div>
              <!-- /.card-header -->
              <div class="card-body">
      
                <table id="example11" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th rowspan="2">Date</th>
                    <th rowspan="2">Income</th>
                    <th rowspan="2">Expence</th>
                    <th colspan="<?php echo count($accounts) + 1; ?>" class="text-center">Account Balance</th>
                  </tr>
                  <tr>
                  
                     <?php foreach($accounts as $acc): ?>
                    <th><?php echo $acc['account_name']; ?></th>
                    <?php endforeach; ?>
                    <th>Total</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php foreach($list as $l): ?>
                  <tr>
                    <td><?php echo $l['transaction_date']; ?></td>
                   <td><button type="button" class="btn btn-primary" onclick="viewIncomeDetails('<?php echo $l['transaction_date']; ?>'); return false">
                    <?php echo $t->getIncomeTotal($l['transaction_date']); ?> </button></td>
                    <td> <button type="button" class="btn btn-primary" onclick="viewExpenseDetails('<?php echo $l['transaction_date']; ?>'); return false">
                    <?php echo $t->getExpenseTotal($l['transaction_date']); ?>
                   </button></td>
                   <?php 
                   $account_balance_details = $t->getAccountBalance($accounts, $l['transaction_date']);
                   foreach($accounts as $acc): ?>
                    <td><?php echo $account_balance_details[$acc['id']]; ?></td>
                    <?php endforeach; ?>
                    <td><?php echo $account_balance_details['total']; ?></td>
                  </tr>
                  <?php endforeach; ?>
               
                  </tbody>
                  <tfoot>
                  </tfoot>
                </table>
              </div>
              
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div class="modal" id="income">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">

           </div>
         </div>
         </div>
        </div>
       <div class="modal" id="expence">
        <div class="modal-dialog">
        <div class="modal-content">
<!-- Modal Header -->
<div class="modal-header">
  <h4 class="modal-title"></h4>
  <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<!-- Modal body -->
<div class="modal-body">
 
  </div>
  </div> 
  </div>
  </div>


<?php $this->load->view('footer'); ?>