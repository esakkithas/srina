<?php $this->load->view('header'); ?>
<?php $this->load->view('sidebar'); ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Services</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Services</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <h3 class="card-title"></h3>
              </div>
            <div class="card">
                <h3></h3>
              <div class="container">
                <!-- Button to Open the Modal -->
           <button type="button" class="btn btn-primary" onclick="removeMessage(); return false" data-toggle="modal" data-target="#add" <?php echo ( $this->session->userdata('is_admin')!=1?'disabled':''); ?>>
               Add+
             </button>   
               </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="services-tbl" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Id</th>
                    <th>Category</th>
                    <th style="width:100px">Name</th>
                    <th>Charges</th>
                    <th>Commission</th>
                    <th>Total</th>
                   <th>action</th> 
                  </tr>
                  </thead>
                  <tbody>
                 
                  </tbody>
                  <tfoot>
                  </tfoot>
                </table>
              </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <!-- The Modal -->
<div class="modal" id="add">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Form</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <h2></h2>
<form  onsubmit="addService(); return false" method="post">
  <div class="form-group">
    <label for="uname">Name*:</label>
    <input type="uname" class="form-control" id="uname"  name="name" required>
    <div class="valid-feedback">Valid.</div>
  </div>
  <div class="form-group">
    <label for="pwd">Charges*:</label>
    <input type="number" class="form-control" id="pwd"  name="fees" required>
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
  </div>
  <div class="form-group">
    <label for="pwd">Commission*:</label>
    <input type="number" class="form-control" id="pwd"  name="commission" required>
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
  </div> 
  
  <div class="ab"><p class="msg" style="color:green;float:left"></p>
  <input type="submit"value="submit" class="btn btn-primary"> </div>
</form>
      </div>
              </div>
              </div>
              </div>

              
<div class="modal" id="edit">
<input type="hidden" name="id">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Form</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <h2></h2>
<form  onsubmit="confirmEditService(); return false" method="post">
<input type="hidden" name="id">


<div class="form-group">
    <label for="pwd">Category*:</label>
    <input list="categorylist" id="answerInput" class="custom-select mb-2" required>
  <datalist id="categorylist">
  <?php foreach($categories_list as $cat): ?>
  <option data-value="<?php echo $cat['id']; ?>" value="<?php echo $cat['name']; ?>"><?php echo $cat['name']; ?> </option>
  <?php endforeach; ?>
    </datalist>
    <input type="hidden" name="category_id" id="answerInput-hidden">
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
  </div>


  <div class="form-group">
    <label for="uname">Name*:</label>
    <input type="uname" class="form-control" id="uname"  name="name" required>
    <div class="valid-feedback">Valid.</div>
  </div>
  <div class="form-group">
    <label for="pwd">Charges*:</label>
    <input type="number" class="form-control charges" id="pwd"  name="fees" required>
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
  </div>
  <div class="form-group">
    <label for="pwd">Commission*:</label>
    <input type="number" class="form-control" id="pwd"  name="commission" required>
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
  </div> 
  <div class="form-group">
    <label for="pswd">Charges Debit Account:</label>

  <select id="assetsList" class="form-control fees_account_id" name="fees_account_id" >
    <option value="">Select</option>
  <?php foreach($accountsList as $acc): ?>
  <option value="<?php echo $acc['id']; ?>"><?php echo $acc['account_name']; ?> </option>
  <?php endforeach; ?>
  </select>

  </div>
  <div class="form-group">
    <label for="pwd">Required Documents</label>
    
    <textarea class="form-control" id="required_documents"  name="required_documents"></textarea>
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
  </div> 
 
  <div class="form-group">
    <label for="pwd">URL</label>
    <input type="url" class="form-control" id="pwd"  name="url">
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
  </div> 
  <div class="form-group">
    <label for="pwd">steps</label>
    <textarea class="form-control" id="steps"  name="steps"></textarea>
    <div class="valid-feedback">Valid.</div>
    <div class="invalid-feedback">Please fill out this field.</div>
  </div> 

  <div class="ab"><p class="msg" style="color:green;float:left"></p>
  <input type="submit"value="submit" class="btn btn-primary"> </div>
</form>
      </div>
              </div>
              </div>
              </div>
            
            <!-- The Modal -->
           <div class="modal" id="view1">
            <div class="modal-dialog modal-xl">
              <div class="modal-content">
                 <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <td class="id"></td>
                </tr>
                
                <tr>
                <th >Name</th>
                <td class="name"></td>
              </tr>
              <tr>
                <th>Charges</th>
                <td class="fees"></td>
              </tr>
              <tr>
                <th>Commission</th>
                <td class="commission"></td>
              </tr> <tr>
                <th>Total Price</th>
                <td class="total"></td>
              </tr>
              
              <tr>
                <th>Required Documents</th>
                <td class="required_documents"></td>
              </tr>
              <?php if($this->session->userdata('is_admin') == 1 ): ?>
              <tr>
                <th>url</th>
                <td class="url"></td>
              </tr>
              
              <tr>
                <th>Steps</th>
                <td class="steps"></td>
              </tr>
              <?php endif; ?>
                </thead>
                <tbody>
                  </tbody>
                  </table>
                  </div>
                  </div>
                  </div>
                  </div>


<?php $this->load->view('footer'); ?>