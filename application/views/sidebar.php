  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="dashboard" class="brand-link">
      <img src="../assets/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Srina CSC</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="../assets/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo  $this->session->userdata('username'); ?></a>
        </div>
      </div>
      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search" disabled>
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <?php if( $this->session->userdata('is_admin') == 1 ): ?>
          <li class="nav-item">
            <a href="Dashboard" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="Income" class="nav-link">
            <i class=" nav-icon fas fa-dollar-sign"></i>
              <p>
                Income
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="Expense" class="nav-link">
            <i class="nav-icon fab fa-apple"></i>
              <p>
                Expense
              </p>
            </a>
</li>
       
          </li>
          <?php endif; ?>
          <li class="nav-item">
                <a href="Categories" class="nav-link">
                <i class="nav-icon fas fa-wrench"></i>
                  <p>Categories</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="Services" class="nav-link">
                <i class="nav-icon fas fa-wrench"></i>
                  <p>Services</p>
                </a>
              </li>
              <?php if( $this->session->userdata('is_admin') == 1 ): ?>
              <li class="nav-item">
                <a href="Customers" class="nav-link">
                <i class="nav-icon fas fa-user-alt"></i>
                  <p>customers</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="Employees" class="nav-link">
                <i class=" nav-icon fas fa-restroom"></i>
                  <p>Employees</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="Assets" class="nav-link">
                <i class="nav-icon fas fa-align-justify"></i>
                  <p>
                    Assets
                  </p>
                </a>
              </li>
              <?php endif; ?>
              <li class="nav-item">
                <a href="Accounts/logout" class="nav-link">
                <i class="fas fa-sync"></i>
                  <p>
                    logout
                  </p>
                </a>
              </li>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>