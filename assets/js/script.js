function login (){
   
    var formdata = $("#loginform").serialize();
    $.ajax({url: "auth", method:'POST',data:formdata,dataType:'json',success: function(result){
        if(result.status == "success"){
            window.location = "../services";
        }else{
            alert("Invalid username/password");
        }
      
    }});
}
function expense (){
    alert("../tables/data.html");
}
function billing (){
    alert("../tables/data.html");
}
function assets (){
    alert("../tables/data.html");
}
function services (){
    alert("../tables/data.html");
}
function customers (){
    alert("../tables/data.html");
}
function employees (){
    alert("../tables/data.html");
}
function removeMessage(){
    $('.modal-body .msg').text('');
}

function viewIncomeDetails(transactionDate){
    
    $.ajax({url: "dashboard/getIncomeDetails", method:'POST',data:{'transactionDate': transactionDate},success: function(result){
        $('.modal-body .msg').text('');
        $('#income').modal('show');
        $('#income .modal-body').html(result);
    }});
}
function viewExpenseDetails(transactionDate){
    
    $.ajax({url: "dashboard/getExpenseDetails", method:'POST',data:{'transactionDate': transactionDate},success: function(result){
        $('.modal-body .msg').text('');
        $('#expence').modal('show');
        $('#expence .modal-body').html(result);
    }});
}
function confirmEditService(){
    var charges = $('.charges').val();
    if(parseInt(charges) > 0){ 
        var fees_account_id = $('.fees_account_id option:selected').val();
        if(fees_account_id == undefined){
            alert('Please select debit account');
            return false;
        }
    }
    $('#edit form :submit').prop('disabled', true);
    var formData = $('#edit form').serialize();
    $.ajax({url: "services/edit", method:'POST', dataType: 'json',data:formData,success: function(result){
        getServicesList();
        $('#edit .msg').html('Updated Successfully');
        $('#edit form :submit').prop('disabled', false);
        // $('#add').modal('hide');
    }});
}

function addService(){
    $('#add form :submit').prop('disabled', true);
    var formData = $('#add form').serialize();
    $.ajax({url: "services/add", method:'POST', dataType: 'json',data:formData,success: function(result){
        getServicesList();
        $('#add form')[0].reset();
        $('#add .msg').html('Added Successfully');
        $('#add form :submit').prop('disabled', false);
        // $('#add').modal('hide');
    }});
}
function viewService(id){  //console.log(servicesList[id])
    // $.ajax({url: "services/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
    //     $('.modal-body .msg').text('');
    //     $('#view1').modal('show');
    //     $('#view1 .id').html(result['data']['id']);
    //     $('#view1 .name').html(result['data']['name']);
    //     $('#view1 .fees').html(result['data']['fees']);
    //     $('#view1 .commission').html(result['data']['commission']);
    //     $('#view1 .total').html(parseInt(result['data']['fees']) + parseInt(result['data']['commission']));
    //     $('#view1 .required_documents').html(result['data']['required_documents']);
    //     $('#view1 .url').html('<a href="'+result['data']['url']+'" target="_blank">'+result['data']['url']+'</a>');
    //     $('#view1 .steps').html(result['data']['steps']);
    // }});
    $('.modal-body .msg').text('');
    $('#view1').modal('show');
    $('#view1 .id').html(servicesList[id]['service_id']);
    $('#view1 .name').html(servicesList[id]['service_name']);
    $('#view1 .fees').html(servicesList[id]['fees']);
    $('#view1 .commission').html(servicesList[id]['commission']);
    $('#view1 .total').html(parseInt(servicesList[id]['fees']) + parseInt(servicesList[id]['commission']));
    $('#view1 .required_documents').html(servicesList[id]['required_documents']);
    $('#view1 .url').html('<a href="'+servicesList[id]['url']+'" target="_blank">'+servicesList[id]['url']+'</a>');
    $('#view1 .steps').html(servicesList[id]['steps']);

}
function editService(id){
    
    $.ajax({url: "services/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
        $('.modal-body .msg').text('');
        $('#edit').modal('show');
        $('#edit input[name=id]').val(result['data']['id']);
        $('#edit input[name=name]').val(result['data']['name']);
        $('#edit input[name=fees]').val(result['data']['fees']);
        $('#edit input[name=commission]').val(result['data']['commission']);
        $('#edit textarea[name=required_documents]').val(result['data']['required_documents']);
        $('#required_documents').summernote('destroy')
        $('#required_documents').summernote({
            height: 400,
        focus: true
          });
        $('#edit textarea[name=steps]').val(result['data']['steps']);
        $('#edit input[name=url]').val(result['data']['url']);
        $('#edit select[name=fees_account_id]').val(result['data']['fees_account_id']);
        // $('#edit #answerInput').val(result['data']['category_id']);
        datalist('answerInput', result['data']['category_id']);
        // $('#answerInput').trigger('input');
    }});
}
function deleteService(id, name){
    if (confirm('Are you sure to delete '+name+'?') == true) { 
         $.ajax({url: "services/delete", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
            getServicesList();
           }});
      }
}
var servicesList = {};
function getServicesList(){
    $.ajax({url: "services/list1", dataType: 'json',success: function(result){
        var html = '';
        result['data'].forEach(element => {
            html += '<tr>'
            html += '<td>'+element['service_id']+'</td>';
            html += '<td>'+element['category_name']+'</td>';
            html += '<td>'+element['service_name']+'</td>';
            html += '<td>'+element['fees']+'</td>';
            html += '<td>'+element['commission']+'</td>';
            html += '<td>'+( parseInt(element['fees']) + parseInt(element['commission']) )+'</td>';
            servicesList[element['service_id']] = element;
            html += '<td><button type="button" class="btn btn-primary" onclick="viewService('+element['service_id']+'); return false">view</button> ';
           html += '<button type="button" class="btn btn-primary" onclick="editService('+element['service_id']+'); return false" '+(isAdmin!=1?'disabled':'')+'>Edit</button> ';
            html += '<button type="button" class="btn btn-primary" onclick="deleteService('+element['service_id']+', \''+element['name']+'\'); return false" '+(isAdmin!=1?'disabled':'')+'>Delete</button></td>'
            html += '</tr>'
        });
        $('#services-tbl tbody').html(html);
        
        if(isAdmin !=1){
            $('#services-tbl').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                "bDestroy": true
              });

        }
      }});
}

function addCategory(){
    $('#add form :submit').prop('disabled', true);
    var formData = $('#add form').serialize();
    var url_string = window.location.href;
    var url = new URL(url_string);
    var c = url.searchParams.get("p");
    $.ajax({url: "categories/add?p="+c, method:'POST', dataType: 'json',data:formData,success: function(result){
        getCategoriesList();
        $('#add form')[0].reset();
        $('#add .msg').html('Added Successfully');
        $('#add form :submit').prop('disabled', false);
        // $('#add').modal('hide');
    }});
}
function viewCategory(id){
    
    $.ajax({url: "categories/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
        $('.modal-body .msg').text('');
        $('#view1').modal('show');
        $('#view1 .id').html(result['data']['id']);
        $('#view1 .name').html(result['data']['name']);
        $('#view1 .fees').html(result['data']['fees']);
        $('#view1 .commission').html(result['data']['commission']);
        $('#view1 .total').html(parseInt(result['data']['fees']) + parseInt(result['data']['commission']));
    }});
}
function editCategory(id){
    
    $.ajax({url: "categories/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
        $('.modal-body .msg').text('');
        $('#edit').modal('show');
        $('#edit input[name=id]').val(result['data']['id']);
        $('#edit input[name=name]').val(result['data']['name']);
        $('#edit input[name=fees]').val(result['data']['fees']);
        $('#edit input[name=commission]').val(result['data']['commission']);
    }});
}
function deleteCategory(id, name){
    if (confirm('Are you sure to delete '+name+'?') == true) { 
         $.ajax({url: "categories/delete", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
            getCategoriesList();
           }});
      }
}
function getCategoriesList(){
    var url_string = window.location.href;
    var url = new URL(url_string);
    var c = url.searchParams.get("p");


    $.ajax({url: "categories/list2?p="+c, dataType: 'json',success: function(result){
        var html = '';
        result['data'].forEach(element => {
            html += '<tr>'
            html += '<td>'+element['id']+'</td>';
            html += '<td><a href="Categories?p='+element['id']+'">'+element['name']+'</a></td>';
            html += '<td>';
            // <button type="button" class="btn btn-primary" onclick="viewCategory('+element['id']+'); return false">view</button> ';
        //    html += '<button type="button" class="btn btn-primary" onclick="editCategory('+element['id']+'); return false">Edit</button> ';
            html += '<button type="button" class="btn btn-primary" onclick="deleteCategory('+element['id']+', \''+element['name']+'\'); return false" '+(isAdmin!=1?'disabled':'')+'>Delete</button></td>'
            html += '</tr>'
        });
        $('#categories-tbl tbody').html(html);
        
       if(isAdmin !=1){
        $('#categories-tbl').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            "bDestroy": true
          });

       }
      }});
}


function addRow(e){
    var length = $('#add1 #serviceslisttbl tbody tr').length;
    console.log(length);
     var temp = $(e).closest('tr').clone();
     temp.find('.service-name').attr('id', 'service-name'+length);
     temp.find('.service-name-hidden').attr('id', 'service-name'+length+'-hidden');
     temp.appendTo("#serviceslisttbl tbody");
     inputEvent();
     calculation();
}
function deleteRow(e){
    $(e).closest('tr').remove();
}
$( document ).ready(function() {
    getServicesList();
    getCategoriesList();
    getcustomersList();
    getincomeList();
    getexpenseList();
    getemployeesList();
    getassetsList();
    // $("#addincome-datetime").val("2013-03-18T13:00");
    // $('datalist#categorylist').change(function() { console.log('test');
    //     // var value = $('input').val(); 
    //     // console.log($('#list-professor [value="' + value + '"]').data('value'));
    // });
    
    inputEvent();
    paymentStatusChanges();
    $('body').bind('cut copy', function(event) {
        event.preventDefault();
    });
});

function inputEvent(){
    var inputlist = document.querySelectorAll('input[list]');
    for (var i = 0, len = inputlist.length; i < len; i++) {
        inputlist[i].addEventListener('input', function(e) {
            var input = e.target;
            // datalist(input.getAttribute('id'),input.value); 
                list = input.getAttribute('list'),
                options = document.querySelectorAll('#' + list + ' option'),
                hiddenInput = document.getElementById(input.getAttribute('id') + '-hidden'),
                inputValue = input.value;
                console.log(options);
            // hiddenInput.value = inputValue;
        
            for(var i = 0; i < options.length; i++) {
                var option = options[i]; 
                if(option.innerText.trim() == inputValue.trim()) { 
                    hiddenInput.value = option.getAttribute('data-value');
                    var r = $('#'+input.getAttribute('id') + '-hidden');
                    r.closest('tr').find('.total-amount').val(option.getAttribute('data-comm'));
                    r.closest('tr').find('.charges').val(option.getAttribute('data-charges'));
                    r.closest('tr').find('.fees_account_id').val(option.getAttribute('data-fees_account_id'));
                    r.closest('tr').find('.quantity').val(1);
                    calculation();
                    break;
                }
            }
        });
      }
}
function paymentStatusChanges(){
    var val = $('.payment_status option:selected').val();
    if(val == 1){
        $('.pending-amt').hide();
        $('.payment-responsible-by').hide();
    }else{
        $('.pending-amt').show();
        $('.payment-responsible-by').show();
        $('.pending-amount').val($('.bill-amount').val());
    }

}

function paymentStatusChangesEdit(){
    var val = $('.payment_status-edit option:selected').val();
    if(val == 1){
        $('.pending-amt-edit').hide();
        $('.payment-responsible-by-edit').hide();
    }else{
        $('.pending-amt-edit').show();
        $('.payment-responsible-by-edit').show();
        $('.pending-amount').val($('.bill-amount').val());
    }

}
function datalist(id, inputValue){
    var input = document.getElementById(id);
    var list = input.getAttribute('list');
    var  options = document.querySelectorAll('#' + list + ' option');
    var hiddenInput = document.getElementById(id + '-hidden');
    // inputValue = input.value;

    // hiddenInput.value = inputValue;
// console.log(options);
    for(var i = 0; i < options.length; i++) {
        var option = options[i]; 
        console.log(option.getAttribute('data-value'));
        console.log(inputValue); 
        if(option.getAttribute('data-value').trim() == inputValue.trim()) { 
            console.log('test');
            console.log(option.getAttribute('data-value'));
            hiddenInput.value = option.getAttribute('data-value');
            input.value =  option.getAttribute('value');
            break;
        }
        console.log('..');
    }
}
function addcustomers(){
    $('#add form :submit').prop('disabled', true);
    var formData = $('#add form').serialize();
    $.ajax({url: "customers/add", method:'POST', dataType: 'json',data:formData,success: function(result){
        getcustomersList();
        $('#add form')[0].reset();
        $('#add .msg').html('Added Successfully');
        $('#add form :submit').prop('disabled', false);
        // $('#add').modal('hide');
    }});
}
function viewcustomers(id){
    
    $.ajax({url: "customers/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
        $('.modal-body .msg').text('');
        $('#view1').modal('show');
        $('#view1 .id').html(result['data']['id']);
        $('#view1 .name').html(result['data']['name']);
        $('#view1 .mobile').html(result['data']['mobile']);
        $('#view1 .address').html(result['data']['address']);
    }});

}
function deletecustomers(id, name){
    if (confirm('Are you sure to delete '+name+'?') == true) { 
         $.ajax({url: "customers/delete", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
             getcustomersList();
          }});
      }
    }
function getcustomersList(){
    $.ajax({url: "customers/list1", dataType: 'json',success: function(result){
        var html = '';
        result['data'].forEach(element => {
            html += '<tr>'
            html += '<td>'+element['id']+'</td>';
            html += '<td>'+element['name']+'</td>';
            html += '<td>'+element['mobile']+'</td>';
            html += '<td>'+element['address']+'</td>';
            html += '<td><button type="button" class="btn btn-primary"onclick="viewcustomers('+element['id']+'); return false">view</button> ';
           // html += '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add">Edit</button> ';
            html += '<button type="button" class="btn btn-primary" onclick="deletecustomers('+element['id']+', \''+element['name']+'\'); return false">Delete</button></td>'
            html += '</tr>'
        });
        $('#customers-tbl tbody').html(html);
        // $('#customers-tbl').DataTable({
        //     "paging": true,
        //     "lengthChange": false,
        //     "searching": true,
        //     "ordering": true,
        //     "info": true,
        //     "autoWidth": false,
        //     "responsive": true,
        //   });
    }});
  }
  function addincome(){
    $('#add1 #serviceslisttbl tbody tr').each(function(index, value) {
        var charges = $(this).find('.charges').val();
        if(parseInt(charges) > 0){ 
            var fees_account_id = $(this).find('.fees_account_id option:selected').val();
            if(fees_account_id == undefined || fees_account_id == ""){
                alert('Please select debit account');
                return false;
            }
        }
    });
    var pending_amount = $('.pending-amount').val();
    if(parseInt(pending_amount) >0){
        var payment_responsible_by = $("#payment_responsible_by-hidden").val();
            if(payment_responsible_by == undefined || payment_responsible_by == ""){
                alert('Please select "Payment Responsible By"');
                return false;
            }
    }

    $('#add1 form :submit').prop('disabled', true);
    var formData = $('#add1 form').serialize();
    $.ajax({url: "income/add", method:'POST', dataType: 'json',data:formData,success: function(result){
        getincomeList();
        $('#add1 form')[0].reset();
        alert('Added Successfully');
        $('#add1 form :submit').prop('disabled', false);
        // $('#add').modal('hide');
    }});
}
function editIncome(id){
    
    $.ajax({url: "income/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
        $('.modal-body .msg').text('');
        $('#editForm').modal('show');
        $('#editForm input[name=id]').val(result['data']['income_id']);
        $('#editForm input[name=datetime]').val(result['data']['datetime'].replace(/\s+/g, 'T'));
        // $('#editForm input[name=customer_id]').val(result['data']['customer_id']);
        datalist('customer-id-edit', result['data']['customer_id']);
        $('#editForm input[name=bill_amount]').val(result['data']['bill_amount']);
        $('#editForm select[name=payment_status]').val(result['data']['payment_status']);
        paymentStatusChangesEdit();
        // console.log('..');
        // console.log(result['data']);
        // console.log('..');
        if(result['data']['payment_status'] == "0"){
            $('#editForm select[name=payment_status]').removeAttr('disabled');
            $('#editForm select[name=account_id]').removeAttr('disabled');
           
            $('#editForm select[name=account_id]').val(result['pending']['account_id']);
            $('#editForm input[name=pending_amount]').val(result['pending']['amount']);
            $('#editForm input[name=payment_responsible_by]').val(result['data']['payment_responsible_by']);
            datalist('payment_responsible_by-edit', result['data']['payment_responsible_by']);
        }
        else{
            $('#editForm select[name=payment_status]').attr('disabled', true);
            $('#editForm select[name=account_id]').attr('disabled', true);
            $('#editForm select[name=account_id]').val(result['paid']['account_id']);
        }
       
        $('#editForm input[name=paper_count]').val(result['data']['paper_count']);
        // $('#editForm .customer').html(result['data']['customer_name']);
        // $('#editForm .bill_amount').html(result['data']['bill_amount']);
        // $('#editForm .pending_amount').html(result['data']['pending_amount']);
        // $('#editForm .payment_status').html(result['data']['payment_status'] == 0 ? 'Pending' : 'Paid');
        // console.log(result['serviceslist']);
        // var tblhtml = '';
           var selector = $('#editForm #serviceslisttbl tbody tr:first');
           selector.remove();
        for( sl in result['serviceslist']) {
            var value = result['serviceslist'][sl];
            var html = selector.clone();
            console.log(html);
            html.find('.service-name').val(value['service_name']);
            html.find('.quantity').val(value['quantity']);
            html.find('.charges').val(value['fees']);
            html.find('.total-amount').val(value['commission']);
            html.find('.total_amount').val(parseInt(value['fees'])+parseInt(value['commission']));
            html.find('.status').val(value['status']);
            html.find('.ref-no').val('');
            html.appendTo("#serviceslisttbl tbody");

            // tblhtml += '<tr><td>'+value['service_name']+'</td><td>'+value['amount']+'</td><td>'+value['status']+'</td><td>'+value['ref_no']+'</td></tr>';
          }
        // //   console.log(tblhtml);
        //   $("#serviceslist tbody").html(tblhtml);
       
    }});
  }
  function confirmEditIncome(){
      var income_id =   $('#editForm input[name=id]').val();
    var val = $('.payment_status-edit option:selected').val();
    var account_id = $('#editForm select[name=account_id]').val();
    if(val == "1"){
        if(account_id == "7"){
            alert('Please select Account');
            return false;
        }
        $.ajax({url: "income/edit", method:'POST', dataType: 'json',data:{account_id:account_id, income_id:income_id},success: function(result){
            getincomeList();
            alert('Updated Successfully');
        }});
    }
  }
  function viewincome(id){
    
    $.ajax({url: "income/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
        $('.modal-body .msg').text('');
        $('#view1').modal('show');
        $('#view1 .id').html(result['data']['id']);
        $('#view1 .datetime').html(result['data']['datetime']);
        $('#view1 .customer').html(result['data']['customer_name']);
        $('#view1 .bill_amount').html(result['data']['bill_amount']);
        $('#view1 .pending_amount').html(result['data']['pending_amount']);
        $('#view1 .payment_status').html(result['data']['payment_status'] == 0 ? 'Pending' : 'Paid');
        $('#view1 .account').html(result['data']['payment_responsible_by']);
        $('#view1 .payment_responsible_by').html(result['data']['payment_responsible_by']);
        console.log(result['serviceslist']);
        var tblhtml = '';
        for( sl in result['serviceslist']) {
            var value = result['serviceslist'][sl];
            tblhtml += '<tr><td>'+value['service_name']+'</td><td>'+value['amount']+'</td><td>'+value['status']+'</td><td>'+value['ref_no']+'</td></tr>';
          }
        //   console.log(tblhtml);
          $("#serviceslist tbody").html(tblhtml);
       
    }});
  }
  function deleteincome(id, name){
    if (confirm('Are you sure to delete id '+id+'?') == true) { 
         $.ajax({url: "income/delete", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
             getincomeList();
          }});
      }
    }

function getincomeList(){
    $.ajax({url: "income/list1", dataType: 'json',success: function(result){
        var html = '';
        result['data'].forEach(element => {
            html += '<tr>'
            html += '<td>'+element['income_id']+'</td>';
            html += '<td>'+element['datetime']+'</td>';
            html += '<td>'+element['customer_name']+'</td>';
            html += '<td>'+element['bill_amount']+'</td>';            
            html += '<td>'+ (element['payment_status']==0?"Pending":"Paid") +'</td>';            
            html += '<td><button style="display:none" type="button" class="btn btn-primary" onclick="viewincome('+element['income_id']+'); return false">view</button> ';
            html += '<button type="button" class="btn btn-primary" onclick="editIncome('+element['income_id']+'); return false">Edit</button> ';
            html += '<button type="button" class="btn btn-primary" onclick="deleteincome('+element['income_id']+', \''+element['name']+'\'); return false">Delete</button></td>'
            html += '</tr>'
        });
        $('#income-tbl tbody').html(html);
        // $('#income-tbl').DataTable({
        //     "paging": true,
        //     "lengthChange": false,
        //     "searching": true,
        //     "ordering": true,
        //     "info": true,
        //     "autoWidth": false,
        //     "responsive": true,
        //   });
      }});
}

function addexpense(){
    $('#add form :submit').prop('disabled', true);
  var formData = $('#add form').serialize();

  $.ajax({url: "expense/add", method:'POST', dataType: 'json',data:formData,success: function(result){
      getexpenseList();
      $('#add form')[0].reset();
      $('#add .msg').html('Added Successfully');
      // $('#add').modal('hide');
      $('#add form :submit').prop('disabled', false);
  }});
}
function viewexpense(id){
    
    $.ajax({url: "expense/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
        $('.modal-body .msg').text('');
        $('#view1').modal('show');
        $('#view1 .id').html(result['data']['id']);
        $('#view1 .datetime').html(result['data']['datetime']);
        $('#view1 .asset_name').html(result['data']['asset_name']);
        $('#view1 .count').html(result['data']['count']);
        $('#view1 .amount').html(result['data']['amount']);
        $('#view1 .additional_info').html(result['data']['additional_info']);
    }});
  }
function deleteexpense(id, name){
    if (confirm('Are you sure to delete id '+id+'?') == true) { 
         $.ajax({url: "expense/delete", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
             getexpenseList();
           }});
      }
    }

function getexpenseList(){
    $.ajax({url: "expense/list1", dataType: 'json',success: function(result){
        var html = '';
        result['data'].forEach(element => {
            html += '<tr>'
            html += '<td>'+element['expense_id']+'</td>';
            html += '<td>'+element['datetime']+'</td>';
            html += '<td>'+element['asset_name']+'</td>';
            html += '<td>'+element['count']+'</td>';
            html += '<td>'+element['amount']+'</td>';
            html += '<td>'+element['additional_info']+'</td>';
            html += '<td>';
             html += '<button type="button" class="btn btn-primary" onclick="viewexpense('+element['expense_id']+'); return false">view</button>';
           // html += '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add">Edit</button> ';
            html += ' <button type="button" class="btn btn-primary" onclick="deleteexpense('+element['expense_id']+', \''+element['name']+'\'); return false" >Delete</button></td>'
            html += '</tr>'
        });
        $('#expense-tbl tbody').html(html);
        // $('#expense-tbl').DataTable({
        //     "paging": true,
        //     "lengthChange": false,
        //     "searching": true,
        //     "ordering": true,
        //     "info": true,
        //     "autoWidth": false,
        //     "responsive": true,
        //   });
      }});
}
/* End Expense */
function addemployees(){
    $('#add form :submit').prop('disabled', true);
    var formData = $('#add form').serialize();
    $.ajax({url: "employees/add", method:'POST', dataType: 'json',data:formData,success: function(result){
        getemployeesList();
        $('#add form')[0].reset();
        $('#add .msg').html('Added Successfully');
        $('#add form :submit').prop('disabled', false);
        // $('#add').modal('hide');
    }});
  }
function viewemployees(id){
    
    $.ajax({url: "employees/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
        $('.modal-body .msg').text('');
        $('#view1').modal('show');
        $('#view1 .id').html(result['data']['id']);
        $('#view1 .name').html(result['data']['name']);
        $('#view1 .mobile').html(result['data']['mobile']);
        $('#view1 .status').html(result['data']['status']);
    }});
  }
function deleteemployees(id, name){
    if (confirm('Are you sure to delete '+name+'?') == true) { 
         $.ajax({url: "employees/delete", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
            getemployeesList();
          }});
      }
    }

function getemployeesList(){
    $.ajax({url: "employees/list1", dataType: 'json',success: function(result){
        var html = '';
        result['data'].forEach(element => {
            html += '<tr>'
            html += '<td>'+element['id']+'</td>';
            html += '<td>'+element['name']+'</td>';
            html += '<td>'+element['mobile']+'</td>';
            html += '<td>'+element['status']+'</td>';
            html += '<td><button type="button" class="btn btn-primary" onclick="viewemployees('+element['id']+'); return false">view</button> ';
           // html += '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add">Edit</button> ';
            html += '<button type="button" class="btn btn-primary"onclick="deleteemployees('+element['id']+', \''+element['name']+'\'); return false">Delete</button></td>'
            html += '</tr>'
        });
        $('#employees-tbl tbody').html(html);
        // $('#employees-tbl').DataTable({
        //     "paging": true,
        //     "lengthChange": false,
        //     "searching": true,
        //     "ordering": true,
        //     "info": true,
        //     "autoWidth": false,
        //     "responsive": true,
        //   });
      }});
    }
      function addassets(){
        $('#add form :submit').prop('disabled', true);
        var formData = $('#add form').serialize();
        $.ajax({url: "assets/add", method:'POST', dataType: 'json',data:formData,success: function(result){
            getassetsList();
            $('#add form')[0].reset();
            $('#add .msg').html('Added Successfully');
            $('#add form :submit').prop('disabled', false);
            // $('#add').modal('hide');
        }});
    }

function viewassets(id){
    
    $.ajax({url: "assets/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
        $('.modal-body .msg').text('');
        $('#view1').modal('show');
        $('#view1 .id').html(result['data']['id']);
        $('#view1 .name').html(result['data']['name']);
        $('#view1 .stock').html(result['data']['stock']);
    }});
  }
  
function deleteassets(id, name){
    if (confirm('Are you sure to delete '+name+'?') == true) { 
         $.ajax({url: "assets/delete", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
             getassetsList();
          }});
      }
    }
function editAsset(id){
    $.ajax({url: "assets/view", method:'POST', dataType: 'json',data:{'id': id},success: function(result){
        $('#edit').modal('show');
        $('#edit input[name=id]').val(result['data']['id']);
        $('#edit input[name=name]').val(result['data']['name']);
        $('#edit input[name=stock]').val(result['data']['stock']);
    }});
}
function confirmEditAssets(){
    $('#edit form :submit').prop('disabled', true);
    var formData = $('#edit form').serialize();
    $.ajax({url: "assets/edit", method:'POST', dataType: 'json',data:formData,success: function(result){
        getassetsList();
        $('#edit .msg').html('Updated Successfully');
        $('#edit form :submit').prop('disabled', false);
        // $('#add').modal('hide');
    }});
}
function getassetsList(){
    $.ajax({url: "assets/list1", dataType: 'json',success: function(result){
        var html = '';
        result['data'].forEach(element => {
            html += '<tr>'
            html += '<td>'+element['id']+'</td>';
            html += '<td>'+element['name']+'</td>';
            html += '<td>'+element['stock']+'</td>';
            html += '<td><button type="button" class="btn btn-primary" onclick="viewassets('+element['id']+'); return false">view</button> ';
            html += '<button type="button" class="btn btn-primary" onclick="editAsset('+element['id']+'); return false">Edit</button> ';
            html += ' <button type="button" class="btn btn-primary" onclick="deleteassets('+element['id']+', \''+element['name']+'\'); return false">Delete</button></td>'
            html += '</tr>'
        });
        $('#assets-tbl tbody').html(html);
        // $('#assets-tbl').DataTable({
        //     "paging": true,
        //     "lengthChange": false,
        //     "searching": true,
        //     "ordering": true,
        //     "info": true,
        //     "autoWidth": false,
        //     "responsive": true,
        //   });
      }});
}
function calculation(){ 
    var bill_amount = 0;
    $('#add1 #serviceslisttbl tbody tr').each(function(index, value) {
        var quantity = $(this).find('.quantity').val();
        var charges = $(this).find('.charges').val();
        var total_amount = $(this).find('.total-amount').val();
        var total = parseInt(quantity) * (parseInt(charges) + parseInt(total_amount));
        bill_amount += total;
        $('.bill-amount').val(bill_amount);
        
        

    });
}


